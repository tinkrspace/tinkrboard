# Examples

## BitmapTest
Tests the LCD. Searches for suitable BMP files on the microSD and displays them

## TBoneTester
Basic tester for TBone. Tests that LEDs, buttons, and joystick are working.
Also tests that it can communicate to the microSD card (sends the report via Serial at 9600bps)
