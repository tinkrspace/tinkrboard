#include <SPI.h>
#include <SD.h>

#define LED1 9
#define LED2 6
#define BUTTON1 4
#define BUTTON2 2
#define JOYSTICK_X 0
#define JOYSTICK_Y 1
#define SPEAKER 3
#define CARDCS (A3)

#define BPM 100
#define TOCK ((60L*1000L/BPM)>>2)

int led1State = 64;
int led2State = 64;
unsigned long nextTock = 0;

int notes[12] = {
  440,
  466,
  494,
  523,
  554,
  587,
  622,
  659,
  698,
  740,
  784,
  831
};

int imperialMarch[] = {
  -2, 4,
  -2, 4,
  -2, 4,
  -6, 3,
  1, 1,
  -2, 4,
  -6, 3,
  1, 1,
  -2, 8,
  5, 4,
  5, 4,
  5, 4,
  6, 3,
  1, 1,
  -3, 4,
  -6, 3,
  1, 1,
  -2, 8
};

int noteCount = sizeof(imperialMarch)/sizeof(int)/2;
int curNote;
int curTocks;
int playing = 0;

File root, file;

void setup() {
  Serial.begin(9600);
  Serial.println();
  Serial.println("In setup()");
  pinMode(BUTTON1, INPUT_PULLUP);
  pinMode(BUTTON2, INPUT_PULLUP);
  pinMode(CARDCS, OUTPUT);
  
  SD.begin(CARDCS);
  root = SD.open("/");
  Serial.print("Root: ");
  Serial.println(root.name());
  
  // Get all entries
  while(file = root.openNextFile()) {
    Serial.print("File: ");
    Serial.println(file.name());
    file.close();
  }
  root.close();
}

void startPlaying() {
  if (playing)
    return;

  playing = 1;
  curNote = 0;
  nextTock = 0;
  playNote();
}

void stopPlaying() {
  if (!playing)
    return;
  
  playing = 0;
  noTone(SPEAKER);
}

void playNote() {
  if (!playing)
    return;

  unsigned long now = millis();
  if (curNote < noteCount && nextTock < now) {
    if (nextTock == 0) {
      nextTock = now + TOCK;
      curTocks = 0;
    } else {
      curTocks++;
      nextTock += TOCK;
    }
      
    int note = imperialMarch[curNote<<1];
    int tocks = imperialMarch[(curNote<<1)+1];
    if (curTocks >= tocks) {
      noTone(SPEAKER);
      
      // Get the next tone
      curNote ++;
      curTocks = 0;
      if (curNote >= noteCount) {
        stopPlaying();
        return;
      }
      
      note = imperialMarch[curNote<<1];
      tocks = imperialMarch[(curNote<<1)+1];
    }
    
    int freqIndex = note % 12;
    if (freqIndex < 0)
      freqIndex += 12;
    int freq = notes[freqIndex];
    if (note < 0)
      while(note < 0) {
        note += 12;
        freq>>=1;
      }
    else if (note >= 12)
      while(note >= 12) {
        note -= 12;
        freq<<=1;
      }
    tone(SPEAKER, freq);
  } else if (curNote >= noteCount)
    stopPlaying();
}

void loop() {
  // Update ledState from joysticks
  led1State = (1<<(analogRead(JOYSTICK_X)>>7))-1;
  led2State = (1<<(analogRead(JOYSTICK_Y)>>7))-1;
  // Read buttons states and reflect on LEDs
  if (!digitalRead(BUTTON1)) {
    analogWrite(LED2, 255);
    startPlaying();
  } else
    analogWrite(LED2, led2State);
    
  if (!digitalRead(BUTTON2)) {
    analogWrite(LED1, 255);
    stopPlaying();
  } else
    analogWrite(LED1, led1State);
  
  playNote();
}
