/***
*
* SlidinLights - TinkRBoard TBone example
*
* This example demonstrates how to smoothly alternate TBone's LEDs
*
* Copyright (c) 2014 TinkRBoard.org
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
* 
***/

// Include TBone's header file
#include <TinkRBoard_TBone.h>

// Initialize TBone, enable leds.
TinkRBoard_TBone tbone(TBONE_LED);

// Setup
void setup() {
  // Nothing to do here
}

int led1 = 0;  // Goes from 0 to 10 and back to 0 and repeats
int slope = HIGH;  // HIGH when going from 0 to 10 and LOW when going from 10 to 0
// Loop
void loop() {
  // Light up the LEDs according to led1's state
  analogWrite(TBONE_D1, map(led1, 0, 10, 0, 255));
  analogWrite(TBONE_D2, map(10-led1, 0, 10, 0, 255));
  
  // Calculate next state
  if (led1 == 10) {
    slope = LOW;  // Don't exceed 10
  } else if (led1 == 0) {
    slope = HIGH;
  }
  
  led1 += slope?1:-1;
  // Delay a short time
  delay(100);
}
