/***
*
* FlashinLights - TinkRBoard TBone example
*
* This example demonstrates how to flash TBone's LEDs alternately
*
* Copyright (c) 2014 TinkRBoard.org
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
* 
***/

// Include TBone's header file
#include <TinkRBoard_TBone.h>

// Initialize TBone, enable leds.
TinkRBoard_TBone tbone(TBONE_LED);

// Setup
void setup() {
  // Nothing to do here
}

int led1 = HIGH;
// Loop
void loop() {
  // Toggle led1 state
  led1 = !led1;
  // Light up the LEDs. LED D1 mirrors led1 variable and LED D2 the inverse
  digitalWrite(TBONE_D1, led1);
  digitalWrite(TBONE_D2, !led1);
  // Turn off LEDs after 100mS flash
  delay(100);
  digitalWrite(led1?TBONE_D1:TBONE_D2, LOW);
  // Delay 900mS before looping, making it flash once a second
  delay(900);
}
