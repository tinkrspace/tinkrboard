/***
*
* ButtonsLeds - TinkRBoard TBone example
*
* This example demonstrates the usage of TBone's switches and LEDs
*
* Pressing button S1 lights up LED D1
* Pressing button S2 lights up LED D2
*
* Copyright (c) 2014 TinkRBoard.org
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
* 
***/

// Include TBone's header file
#include <TinkRBoard_TBone.h>

// Initialize TBone, enable buttons and leds
TinkRBoard_TBone tbone(TBONE_SW | TBONE_LED);

// Setup
void setup() {
  // Nothing to do here
}

// Loop
void loop() {
  // Get switches' status
  int s1 = digitalRead(TBONE_S1);
  int s2 = digitalRead(TBONE_S2);
  
  // Light up the corresponding LEDs
  digitalWrite(TBONE_D1, s1?LOW:HIGH);  // If switch is LOW make LED HIGH and vice versa
  digitalWrite(TBONE_D2, s2?LOW:HIGH);
}
