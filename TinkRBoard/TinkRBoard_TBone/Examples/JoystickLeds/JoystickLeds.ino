/***
*
* JoystickLeds - TinkRBoard TBone example
*
* This example demonstrates the usage of TBone's joystick and LEDs
*
* Moving the joystick horizontally controls LED D1's brightness
* Moving the joystick vertically controls LED D2's brightness
*
* LED brightness is controlled using PWM (analogWrite)
*
* Copyright (c) 2014 TinkRBoard.org
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
* 
***/

// Include TBone's header file
#include <TinkRBoard_TBone.h>

// Initialize TBone, enable leds. Joystick is always enabled
TinkRBoard_TBone tbone(TBONE_LED);

// Setup
void setup() {
  // Nothing to do here
}

// analog limits. Initialize to 0 and do autocalibration in loop()
int vMin = 0;
int vMax = 0;

// Loop
void loop() {
  // Get joystick's status
  int x = analogRead(TBONE_X);
  int y = analogRead(TBONE_Y);
  // Calibrate analog limits
  if (x > vMax) {
    vMax = x;
  }
  if (y > vMax) {
    vMax = y;
  }
  
  // Adjust the corresponding LED's brightness
  analogWrite(TBONE_D1, map(x, vMin, vMax, 0, 255));  // Brightness is between 0 and 255
  analogWrite(TBONE_D2, map(y, vMin, vMax, 0, 255));
}
