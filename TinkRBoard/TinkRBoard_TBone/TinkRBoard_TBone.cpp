/***
	TinkRBoard_TBone
	
	Library for TinkRBoard ONE (TBone)
	Copyright (c) 2014 TinkRBoard.org
***/

#include "TinkRBoard_TBone.h"

TinkRBoard_TBone::TinkRBoard_TBone(int init) {
	if (init & TBONE_SW) {	// Init switches pins
#if (ARDUINO >= 100)
		pinMode(TBONE_S1, INPUT_PULLUP);
		pinMode(TBONE_S2, INPUT_PULLUP);
#else
		pinMode(TBONE_S1, INPUT);
		digitalWrite(TBONE_S1, HIGH);
		pinMode(TBONE_S2, INPUT);
		digitalWrite(TBONE_S2, HIGH);
#endif
	}
	
	if (init & TBONE_LED) {	// Init LED pins
		pinMode(TBONE_D1, OUTPUT);
		digitalWrite(TBONE_D1, LOW);
		pinMode(TBONE_D2, OUTPUT);
		digitalWrite(TBONE_D2, LOW);
	}
	
	if (init & TBONE_LCD) {	// Init LCD pins
		pinMode(TBONE_LCD_CS, OUTPUT);
		pinMode(TBONE_LCD_RST, OUTPUT);
		pinMode(TBONE_LCD_DC, OUTPUT);
		pinMode(TBONE_BL, OUTPUT);
	}
	
	if (init & TBONE_SD) {	// Init MicroSD pins
		pinMode(TBONE_SD_CS, OUTPUT);
	}
	
	if (init & TBONE_RF) {	// Init RF module pins
		pinMode(TBONE_RF_CS, OUTPUT);
		pinMode(TBONE_RF_EN, OUTPUT);
	}
}