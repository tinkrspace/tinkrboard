/***
	TinkRBoard_TBone
	
	Library for TinkRBoard ONE (TBone)
	Copyright (c) 2014 TinkRBoard.org
***/

#ifndef TinkRBoard_TBone_h
#define TinkRBoard_TBone_h

#if (ARDUINO >= 100)
	#include "Arduino.h"
#else
	#include "WProgram.h"
#endif

// Init Constants
#define TBONE_SW	1
#define TBONE_LED	2
#define TBONE_LCD	4
#define TBONE_SD	8
#define TBONE_RF	16

// Digital Inputs
#define TBONE_S1	4
#define TBONE_S2	2

// Analog Inputs
#define TBONE_X		0
#define	TBONE_Y		1

// Outputs
#define TBONE_D1	9
#define TBONE_D2	6
#define TBONE_BL	5
#define TBONE_SPKR	3

// Controls
#define TBONE_LCD_CS	A2
#define TBONE_LCD_RST	7
#define TBONE_LCD_DC	8
#define TBONE_SD_CS		A3
#define TBONE_RF_CS		A4
#define TBONE_RF_EN		A5

class TinkRBoard_TBone {
	public:
		TinkRBoard_TBone(int = 0);
	private:
};

#endif
